# Rails Dummy

This is the **Rails** version of **Dummy Api**. **Dummy Api** is a simple basic api with dummy content.
            
It offer basic features like :
 - User authentication with phone number
 - Create articles
 - Comment articles
 - Bookmark articles