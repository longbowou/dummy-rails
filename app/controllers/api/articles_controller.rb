class Api::ArticlesController < ApplicationApiController
  before_action :check_request_token, :setup_pagination

  def index
    @articles = Article.includes(:user, :users_bookmarks, :users_likes).order(created_at: :desc).page(@page).per(@per_page)
  end

  def comments
    return render json: {message: 'Article not found', response_status: 400} unless Article.where(id: params[:article_id]).exists?

    @comments = ArticleComment.includes(:user).where(article_id: params[:article_id]).order(created_at: :desc).page(@page).per(@per_page)
  end
end

