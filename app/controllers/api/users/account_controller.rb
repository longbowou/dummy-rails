class Api::Users::AccountController < ApplicationApiController
  before_action :check_request_token, :validate_phone_number, :authenticate_user, :setup_pagination

  def bookmarks
    @user_bookmarks = UserBookmark.includes(:article).where(user_id: @user.id).order(created_at: :desc).page(@page).per(@per_page)

    @articles = @user_bookmarks.map {|user_bookmark| user_bookmark.article}
  end
end