class Api::Users::ArticlesController < ApplicationApiController
  before_action :check_request_token, :validate_phone_number, :authenticate_user

  def create
    params.permit :phone_number, :encrypted_password, :article_title, :article_description, :article_image_url

    return render json: {message: 'Article title must be filled.', response_status: 400} if params['article_title'].nil? and params['article_title'].blank?

    @article = Article.create title: params[:article_title],
                              description: params[:article_description],
                              image: params[:article_image_url],
                              user_id: @user.id,
                              api_id: @api.id

    @message = "Article #{@article.title} successful created."
  end

  def comment
    params.permit :phone_number, :encrypted_password, :article_id, :comment_content

    return render json: {message: 'Article id must be filled.', response_status: 400} if params['article_id'].nil? and params['article_id'].blank?

    return render json: {message: 'Comment content must be filled.', response_status: 400} if params['comment_content'].nil? and params['comment_content'].blank?

    @article = Article.includes(:user).find params[:article_id]

    return render json: {message: 'Article not found.', response_status: 400} if @article.nil?

    @comment = ArticleComment.create content: params['comment_content'],
                                     user_id: @user.id,
                                     article_id: params[:article_id],
                                     api_id: @api.id

    @bookmarked = UserBookmark.where(user_id: @user.id).where(article_id: @article.id).exists? ? 1 : 0

    @article.reload

    @message = 'You successful comment this article.'
  end

  def like_dislike
    params.permit :phone_number, :encrypted_password, :article_id

    return render json: {message: 'Article id must be filled.', response_status: 400} if params['article_id'].nil? and params['article_id'].blank?

    @article = Article.includes(:user, :users_bookmarks, :users_likes).find params[:article_id]

    return render json: {message: 'Article not found.', response_status: 400} if @article.nil?

    @article_like = ArticleLike.where(user_id: @user.id).where(article_id: @article.id).first

    if @article_like.nil?
      ArticleLike.create user_id: @user.id,
                         article_id: @article.id,
                         api_id: @api.id
    else
      @article_like.destroy
    end

    @article.reload
  end

  def bookmark_un_bookmark
    params.permit :phone_number, :encrypted_password, :article_id

    return render json: {message: 'Article id must be filled.', response_status: 400} if params['article_id'].nil? and params['article_id'].blank?

    @article = Article.includes(:user, :users_bookmarks, :users_likes).find params[:article_id]

    return render json: {message: 'Article not found.', response_status: 400} if @article.nil?

    @user_bookmark = UserBookmark.where(user_id: @user.id).where(article_id: @article.id).first
    if @user_bookmark.nil?
      @user_bookmark = UserBookmark.create user_id: @user.id,
                                           article_id: params[:article_id],
                                           api_id: @api.id
    else
      @user_bookmark.destroy
    end
  end
end