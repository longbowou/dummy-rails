class Api::Users::AuthController < ApplicationApiController
  before_action :check_request_token, :validate_phone_number

  def identification
    params.permit :phone_number

    return render json: {message: "Welcome #{params[:phone_number]}. login with your credentials.", response_status: 200} if User.where(phone_number: (params[:phone_number])).exists?

    render json: {message: "Welcome #{params[:phone_number]}. Please register with your credentials.", response_status: 201}
  end

  def login
    params.permit :phone_number, :password

    @user = User.find_by_phone_number params[:phone_number]

    return render json: {message: "You #{params[:phone_number]} must register with your credentials first.", response_status: 400} if @user.nil?

    return render json: {message: 'Incorrect credentials.', response_status: 400} unless @user.authenticate(params[:password])

    @message = "Welcome #{@user.full_name} #{@user.phone_number}. Enjoy #{APP_NAME}."
  end

  def register
    params.permit :phone_number, :full_name, :password, :password_confirmation, :email

    return render json: {message: 'Password and password confirmation don\'t match.', response_status: 400} unless params[:password].eql? params[:password_confirmation]

    return render json: {message: "You #{params[:phone_number]} must login with your credentials instead.", response_status: 400} if User.where(phone_number: params[:phone_number]).exists?

    @user = User.create phone_number: params[:phone_number], full_name: params[:full_name], password: params[:password], email: params[:email], api_id: @api.id

    @message = "Welcome #{@user.full_name} #{@user.phone_number}. Enjoy #{APP_NAME}."
  end
end
