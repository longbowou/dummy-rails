class ApplicationApiController < ActionController::API
  APP_NAME = 'Dummy App 😜'.freeze

  private

  def check_request_token
    return render json: {message: 'Undefined authorization token.', response_status: 400} if request.headers['Authorization'].nil?

    @api = Api.find_by_app_token request.headers['Authorization'].split(' ')[1]

    return render json: {message: 'Incorrect app token.', response_status: 400} if @api.nil?

    request.format = :json
  end

  def authenticate_user
    return render json: {message: 'Undefined phone number or encrypted password.', response_status: 400} if params[:phone_number].nil? and params[:encrypted_password].nil?

    @user = User.find_by_phone_number params[:phone_number]

    return render json: {message: 'Incorrect credentials.', response_status: 400} if @user.nil?

    render json: {message: 'Incorrect credentials.', response_status: 400} unless @user.password_digest.eql? params[:encrypted_password]
  end

  def validate_phone_number
    render json: {message: 'Incorrect phone number.', response_status: 400} unless Phonelib.valid? params[:phone_number]
  end

  def setup_pagination
    @per_page = params[:per_page].nil? ? 10 : params[:per_page].to_i < 1 ? 1 : params[:per_page].to_i
    @page = params[:at_page].nil? ? 1 : params[:at_page].to_i < 1 ? 1 : params[:at_page].to_i
  end
end
