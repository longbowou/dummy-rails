class HomeController < ApplicationController

  def index
  end

  def register_api
    @api = Api.new
  end

  def store_api
    @api = Api.new(api_params)
    @api.app_token = SecureRandom.uuid

    if @api.save
      return redirect_to api_dashboard_url(app_token: @api)
    end

    render 'home/register_api'
  end

  def identificate_api
    params.permit(:email, :password)

    unless params[:email].nil? and params[:password].nil?
      @api = Api.find_by_email params[:email]

      unless @api.nil?
        return redirect_to api_dashboard_url(app_token: @api) if @api.authenticate(params[:password])
      end

      @message = "These credentials don't match our records."
    end
  end

  def api_dashboard
    @api = Api.find_by_app_token params[:app_token]

    return render_404 if @api.nil?

    @api_last_user = User.order(created_at: :desc).where(api_id: @api.id).select(:created_at).first
    @api_last_article = Article.order(created_at: :desc).where(api_id: @api.id).select(:created_at).first
    @api_last_article_comment = ArticleComment.order(created_at: :desc).where(api_id: @api.id).select(:created_at).first
    @api_last_article_like = ArticleLike.order(created_at: :desc).where(api_id: @api.id).select(:created_at).first
    @api_last_user_bookmark = UserBookmark.order(created_at: :desc).where(api_id: @api.id).select(:created_at).first
  end

  def api_documentation

  end

  private

  def api_params
    params.require(:api).permit(:full_name, :app_name, :app_description, :email, :password, :password_confirmation)
  end
end
