class Api < ApplicationRecord
  has_secure_password
  validates :email, :full_name, :app_name, :app_description, :password, presence: true
  validates :email, uniqueness: true
  validates :password, confirmation: true, on: :create

  def to_param
    app_token
  end
end
