class Article < ApplicationRecord
  belongs_to :api, counter_cache: 'articles_count'
  belongs_to :user, counter_cache: 'articles_count'
  has_many :article_comments
  has_and_belongs_to_many :users_likes, -> {select 'users.id'}, join_table: :article_likes, class_name: "User"
  has_and_belongs_to_many :users_bookmarks, -> {select 'users.id'}, join_table: :user_bookmarks, class_name: "User"

  def created_at_timestamp
    created_at.to_i * 1000
  end
end
