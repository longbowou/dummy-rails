class ArticleComment < ApplicationRecord
  belongs_to :article, counter_cache: 'comments_count'
  belongs_to :api, counter_cache: 'articles_comments_count'
  belongs_to :user

  def created_at_timestamp
    created_at.to_i * 1000
  end
end
