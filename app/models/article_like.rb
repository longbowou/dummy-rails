class ArticleLike < ApplicationRecord
  belongs_to :article, counter_cache: :likes_count
  belongs_to :api, counter_cache: :articles_likes_count
end
