class User < ApplicationRecord
  has_secure_password
  belongs_to :api, counter_cache: 'users_count'
  has_many :articles
  has_many :user_bookmarks

  def created_at_timestamp
    created_at.to_i * 1000
  end
end
