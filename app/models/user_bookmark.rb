class UserBookmark < ApplicationRecord
  belongs_to :api, counter_cache: 'users_bookmarks_count'
  belongs_to :article, counter_cache: 'bookmarks_count'
  belongs_to :user
end
