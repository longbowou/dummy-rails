json.next @next_page
json.previous @previous_page
json.articles do
  json.array! @articles, partial: 'articles/article', as: :article
end
