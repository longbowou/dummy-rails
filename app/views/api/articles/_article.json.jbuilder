json.extract! article, :id, :title, :description, :image, :user_id, :comments_count, :likes_count
json.created_at article.created_at_timestamp
json.users_likes_ids article.users_like_ids.join ' '
json.users_bookmarks_ids article.users_bookmark_ids.join ' '
json.author article.user.full_name
