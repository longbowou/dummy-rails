json.response_status 200
json.comments do
  json.array! @comments, partial: 'api/articles/comment', as: :comment
end
json.next_at_page_number @comments.next_page
json.previous_at_page_number @comments.prev_page