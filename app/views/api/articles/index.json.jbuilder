json.response_status 200
json.articles do
  json.array! @articles, partial: 'api/articles/article', as: :article
end
json.next_at_page_number @articles.next_page
json.previous_at_page_number @articles.prev_page
