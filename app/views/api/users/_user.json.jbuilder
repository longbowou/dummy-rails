json.extract! user, :id, :full_name, :phone_number, :email, :articles_count
json.created_at user.created_at_timestamp
json.encrypted_password user.password_digest
