json.response_status 200
json.articles do
  json.array! @articles, partial: 'api/users/articles/article', as: :article, locals: {user: @user, bookmarked: 1}
end
json.next_at_page_number @user_bookmarks.next_page
json.previous_at_page_number @user_bookmarks.prev_page