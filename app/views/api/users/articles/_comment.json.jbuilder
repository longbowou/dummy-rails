json.extract! comment, :id, :content, :user_id, :article_id
json.created_at comment.created_at_timestamp
json.author user.full_name
