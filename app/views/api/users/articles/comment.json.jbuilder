json.response_status 200
json.message @message
json.article do
  json.partial! 'api/users/articles/article', article: @article, user: @article.user, bookmarked: @bookmarked
end
json.comment do
  json.partial! 'api/users/articles/comment', comment: @comment, user: @user
end