json.response_status 200
json.message @message
json.user do
  json.partial! 'api/users/user', user: @user
end
json.article do
  json.partial! 'api/users/articles/article', article: @article, user: @user, bookmarked: 0
end