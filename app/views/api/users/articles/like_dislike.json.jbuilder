json.response_status 200
json.article do
  json.partial! 'api/users/articles/article', article: @article, user: @article.user, bookmarked: @bookmarked
end