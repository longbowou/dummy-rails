json.response_status 200
json.message @message
json.user do
  json.partial! 'api/users/user', user: @user
end