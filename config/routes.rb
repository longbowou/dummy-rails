Rails.application.routes.draw do

  root to: 'home#index'

  get 'api-registration', to: 'home#register_api'

  post 'api-registration', to: 'home#store_api'

  get 'api-identification', to: 'home#identificate_api'

  post 'api-identification', to: 'home#identificate_api'

  get 'app/:app_token', to: 'home#api_dashboard', as: 'api_dashboard'

  get 'api-documentation', to: 'home#api_documentation'

  # resources :article_comments
  # resources :articles
  # resources :apis
  # resources :users

  namespace 'api' do
    get 'articles', to: 'articles#index', as: :articles

    get 'articles/:article_id/comments', to: 'articles#comments', as: :article_comments

    namespace 'users' do
      post 'identification', to: 'auth#identification'

      post 'login', to: 'auth#login'

      post 'register', to: 'auth#register'

      scope '/articles' do
        post '/create', to: 'articles#create', as: :articles_submit

        post '/like_dislike', to: 'articles#like_dislike', as: :articles_like_dislike

        post '/bookmark_un_bookmark', to: 'articles#bookmark_un_bookmark', as: :articles_bookmark_un_bookmark

        post '/comment', to: 'articles#comment', as: :articles_comments_submit
      end

      scope '/account' do
        post '/bookmarks', to: 'account#bookmarks'
      end
    end
  end
end
