class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone_number, null: false
      t.string :full_name, null: false
      t.string :email
      t.string :password_digest
      t.bigint :articles_count, default: 0
      t.bigint :api_id, null: false

      t.index :phone_number, unique: true
      t.index :email, unique: true

      t.timestamps
    end
  end
end
