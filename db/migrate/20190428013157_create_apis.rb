class CreateApis < ActiveRecord::Migration[5.2]
  def change
    create_table :apis do |t|
      t.string :email, null: false
      t.string :full_name, null: false
      t.string :app_token, null: false
      t.string :app_name, null: true
      t.text :app_description
      t.bigint :articles_count, default: 0
      t.bigint :articles_likes_count, default: 0
      t.bigint :articles_comments_count, default: 0
      t.bigint :users_count, default: 0
      t.bigint :users_bookmarks_count, default: 0
      t.string :password_digest

      t.index :email, unique: true

      t.timestamps
    end
  end
end
