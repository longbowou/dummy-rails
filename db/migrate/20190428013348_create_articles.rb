class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.text :description
      t.string :image
      t.bigint :user_id, null: false
      t.bigint :likes_count, default: 0
      t.bigint :bookmarks_count, default: 0
      t.bigint :comments_count, default: 0
      t.bigint :api_id, null: false

      t.timestamps
    end
  end
end
