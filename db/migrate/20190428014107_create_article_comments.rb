class CreateArticleComments < ActiveRecord::Migration[5.2]
  def change
    create_table :article_comments do |t|
      t.text :content, null: false
      t.bigint :article_id, null: false
      t.bigint :user_id, null: false
      t.bigint :api_id, null: false

      t.timestamps
    end
  end
end
