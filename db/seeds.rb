# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Api.create email: 'dummyapi@dummyapi.com',
           full_name: 'dummy',
           app_token: '71909913a476e19e46a8ca524ba59caa8547f8cb4736639f9e7a8a627eefb2ce',
           app_name: 'App test',
           app_description: 'App description',
           password: 'password'

50.times do
  User.create phone_number: Faker::PhoneNumber.phone_number,
              full_name: "#{Faker::Name.first_name} #{Faker::Name.last_name}",
              email: Faker::Internet.email,
              password: :password,
              api_id: 1

  Article.create title: Faker::Book.author,
                 description: Faker::Lorem.paragraphs(5),
                 image: Faker::LoremPixel.image("300x300"),
                 user_id: Faker::Number.between(1, 50),
                 api_id: 1
end

50.times do
  20.times do
    ArticleComment.create content: Faker::Lorem.paragraph,
                          article_id: Faker::Number.between(1, 50),
                          user_id: Faker::Number.between(1, 50),
                          api_id: 1
  end

  ArticleLike.create article_id: Faker::Number.between(1, 50),
                     user_id: Faker::Number.between(1, 50),
                     api_id: 1
end