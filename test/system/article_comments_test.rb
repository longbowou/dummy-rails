require "application_system_test_case"

class ArticleCommentsTest < ApplicationSystemTestCase
  setup do
    @article_comment = article_comments(:one)
  end

  test "visiting the index" do
    visit article_comments_url
    assert_selector "h1", text: "Article Comments"
  end

  test "creating a Article comment" do
    visit article_comments_url
    click_on "New Article Comment"

    fill_in "Content", with: @article_comment.content
    click_on "Create Article comment"

    assert_text "Article comment was successfully created"
    click_on "Back"
  end

  test "updating a Article comment" do
    visit article_comments_url
    click_on "Edit", match: :first

    fill_in "Content", with: @article_comment.content
    click_on "Update Article comment"

    assert_text "Article comment was successfully updated"
    click_on "Back"
  end

  test "destroying a Article comment" do
    visit article_comments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article comment was successfully destroyed"
  end
end
